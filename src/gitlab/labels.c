/*
 * Copyright 2022-2025 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gcli/gitlab/config.h>
#include <gcli/gitlab/labels.h>
#include <gcli/gitlab/repos.h>
#include <gcli/json_gen.h>
#include <gcli/json_util.h>

#include <templates/gitlab/labels.h>

#include <pdjson/pdjson.h>

int
gitlab_get_labels(struct gcli_ctx *ctx, struct gcli_path const *const path,
                  int const max, struct gcli_label_list *const out)
{
	char *url = NULL;
	int rc = 0;
	struct gcli_fetch_list_ctx fl = {
		.listp = &out->labels,
		.sizep = &out->labels_size,
		.max = max,
		.parse = (parsefn)(parse_gitlab_labels),
	};

	*out = (struct gcli_label_list) {0};

	rc = gitlab_repo_make_url(ctx, path, &url, "/labels");
	if (rc < 0)
		return rc;

	return gcli_fetch_list(ctx, url, &fl);
}

int
gitlab_create_label(struct gcli_ctx *ctx, struct gcli_path const *const path,
                    struct gcli_label *const label)
{
	char *url = NULL, *payload = NULL;
	int rc = 0;
	struct gcli_fetch_buffer buffer = {0};
	struct gcli_jsongen gen = {0};
	struct json_stream stream = {0};

	/* Generate URL */
	rc = gitlab_repo_make_url(ctx, path, &url, "/labels");
	if (rc < 0)
		return rc;

	/* Generate payload */
	gcli_jsongen_init(&gen);
	gcli_jsongen_begin_object(&gen);
	{
		char *colour_string = NULL;

		gcli_jsongen_objmember(&gen, "name");
		gcli_jsongen_string(&gen, label->name);

		colour_string = sn_asprintf("#%06X", label->colour & 0xFFFFFF);
		gcli_jsongen_objmember(&gen, "color");
		gcli_jsongen_string(&gen, colour_string);
		free(colour_string);

		gcli_jsongen_objmember(&gen, "description");
		gcli_jsongen_string(&gen, label->description);
	}
	gcli_jsongen_end_object(&gen);

	payload = gcli_jsongen_to_string(&gen);
	gcli_jsongen_free(&gen);

	rc = gcli_fetch_with_method(ctx, "POST", url, payload, NULL, &buffer);

	if (rc == 0) {
		json_open_buffer(&stream, buffer.data, buffer.length);
		json_set_streaming(&stream, 1);
		parse_gitlab_label(ctx, &stream, label);
		json_close(&stream);
	}

	free(payload);
	free(url);
	gcli_fetch_buffer_free(&buffer);

	return rc;
}

static int
gitlab_label_make_url(struct gcli_ctx *ctx, struct gcli_path const *const path,
                      char **url, char const *const fmt, ...)
{
	int rc = 0;
	va_list vp;
	char *suffix = NULL;

	va_start(vp, fmt);
	suffix = sn_vasprintf(fmt, vp);
	va_end(vp);

	switch (path->kind) {
	case GCLI_PATH_DEFAULT: {
		char *e_owner, *e_repo;

		e_owner = gcli_urlencode(path->as_default.owner);
		e_repo = gcli_urlencode(path->as_default.repo);

		*url = sn_asprintf("%s/projects/%s%%2F%s/labels/%"PRIid"%s",
		                   gcli_get_apibase(ctx), e_owner, e_repo,
		                   path->as_default.id, suffix);

		gcli_clear_ptr(&e_owner);
		gcli_clear_ptr(&e_repo);
	} break;
	case GCLI_PATH_NAMED: {
		char *e_owner, *e_repo, *e_label;

		e_owner = gcli_urlencode(path->as_named.owner);
		e_repo = gcli_urlencode(path->as_named.repo);
		e_label = gcli_urlencode(path->as_named.id);

		*url = sn_asprintf("%s/projects/%s%%2F%s/labels/%s%s",
		                   gcli_get_apibase(ctx), e_owner, e_repo,
		                   e_label, suffix);

		gcli_clear_ptr(&e_owner);
		gcli_clear_ptr(&e_repo);
		gcli_clear_ptr(&e_label);
	} break;
	case GCLI_PATH_URL: {
		*url = sn_asprintf("%s%s", path->as_url, suffix);
	} break;
	default: {
		rc = gcli_error(ctx, "unsupported path kind for GitLab labels");
	} break;
	}

	gcli_clear_ptr(&suffix);

	return rc;
}

int
gitlab_delete_label(struct gcli_ctx *ctx, struct gcli_path const *const path)
{
	char *url = NULL;
	int rc = 0;

	rc = gitlab_label_make_url(ctx, path, &url, "");
	if (rc == 0)
		rc = gcli_fetch_with_method(ctx, "DELETE", url, NULL, NULL, NULL);

	gcli_clear_ptr(&url);

	return rc;
}

int
gitlab_get_label(struct gcli_ctx *ctx, struct gcli_path const *const path,
                 struct gcli_label *const out)
{
	char *url;
	int rc = 0;
	struct gcli_fetch_buffer buffer = {0};
	struct json_stream parser = {0};

	rc = gitlab_label_make_url(ctx, path, &url, "");
	if (rc < 0)
		return rc;

	rc = gcli_fetch(ctx, url, NULL, &buffer);
	if (rc == 0) {
		json_open_buffer(&parser, buffer.data, buffer.length);
		json_set_streaming(&parser, true);
		parse_gitlab_label(ctx, &parser, out);
		json_close(&parser);
	}

	gcli_fetch_buffer_free(&buffer);
	gcli_clear_ptr(&url);

	return rc;
}

static int
gitlab_label_update_property(struct gcli_ctx *ctx, struct gcli_path const *path,
                             char const *const propname, char const *const val)
{
	char *e_val = NULL, *url = NULL;
	int rc = 0;

	e_val = gcli_urlencode(val);

	rc = gitlab_label_make_url(ctx, path, &url, "?%s=%s", propname, e_val);

	if (rc == 0) {
		rc = gcli_fetch_with_method(ctx, "PUT", url, NULL, NULL, NULL);
	}

	gcli_clear_ptr(&url);
	gcli_clear_ptr(&e_val);

	return rc;
}

int
gitlab_label_set_title(struct gcli_ctx *ctx, struct gcli_path const *const path,
                       char const *const new_name)
{
	return gitlab_label_update_property(ctx, path, "new_name", new_name);
}

int
gitlab_label_set_description(struct gcli_ctx *ctx,
                             struct gcli_path const *const path,
                             char const *const new_description)
{
	return gitlab_label_update_property(
		ctx, path, "description", new_description);
}

int
gitlab_label_set_colour(struct gcli_ctx *ctx,
                        struct gcli_path const *const path,
                        uint32_t const colour)
{
	char *colour_string = NULL;
	int rc = 0;

	colour_string = sn_asprintf("#%06X", colour & 0xFFFFFF);
	rc = gitlab_label_update_property(ctx, path, "color", colour_string);

	gcli_clear_ptr(&colour_string);

	return rc;
}
